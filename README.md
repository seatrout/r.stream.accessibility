# r.stream.accessibility

A GRASS GIS module for assessing the accessibility of river networks for
anadromous fish species https://seatrout.gitlab.io/r.stream.accessibility/