#!/usr/bin/env python

"""
MODULE:    r.stream.accessibility

AUTHOR(S): Stefan Blumentrath <stefan.blumentrath nina no>

PURPOSE:   Assess the accessibility of river networks for anadromous fish species

COPYRIGHT: (C) 2019 by StefaBlumentrath and the GRASS Development Team

This program is free software under the GNU General Public
License (>=v2). Read the file COPYING that comes with GRASS
for details.
"""

#%module
#% description: Assess the accessibility of river networks for anadromous fish species
#% keyword: raster
#% keyword: stream
#% keyword: network
#% keyword: accessibility
#% keyword: barrier
#%end
#%option G_OPT_R_INPUT
#% key: cross_n
#% required: no
#% label: Number of road crossings
#% description: Raster map with number of road crossings downstreams
#%end
#%option G_OPT_R_INPUT
#% key: dist 
#% required: yes
#% label: Distance from outlet
#% description: Raster map with distance from outlet in meter
#%end
#%option G_OPT_R_OUTPUT
#% required: yes
#% label: Likelihood of accessibility
#% description: Raster output map with likelihood of accessibility
#%end
#%option
#% key: cross_n_cen
#% label: Mean of number of crossings downstreams in underlying model
#% type: double
#% required: yes
#% answer: 5.5
#% guisection: Coefficients
#%end
#%option
#% key: cross_n_sca
#% type: double
#% required: yes
#% answer: 3.02765
#% guisection: Coefficients
#%end
#%option
#% key: dist_cen
#% label: Mean of distance from outlet in underlying model
#% type: double
#% required: yes
#% answer: 10.43795
#% guisection: Coefficients
#%end
#%option
#% key: dist_sca
#% type: double
#% required: yes
#% answer: 15.43328
#% guisection: Coefficients
#%end
#%option
#% key: a
#% label: Intercept in underlying model
#% type: double
#% required: yes
#% answer: 7.05
#% guisection: Coefficients
#%end
#%option
#% key: b1
#% label: Effect of number of crossings downstreams in underlying model
#% type: double
#% required: yes
#% answer: -8.701
#% guisection: Coefficients
#%end
#%option
#% key: b2
#% label: Effect of distance from outlet in underlying model
#% type: double
#% required: yes
#% answer: 13.688
#% guisection: Coefficients
#%end
#%option
#% key: model_type
#% label: Underlying modelto use in accessibility computation
#% type: string
#% options: all_crossing,cuverts,custom_model
#% required: yes
#% answer: all_crossing
#% guisection: Coefficients
#%end


import sys

import grass.script as gscript


def main():
    """Do the main work"""
    options, flags = gscript.parser()
    # TRANSFORM CrossN AND Dist
    cross_n_scale = '( {cross_n} - {cross_n_cen} ) / {cross_n_sca}'.format(**options)
    dist_scale = '( {dist} - {dist_cen} ) / {dist_sca}'.format(**options)

    # PREDICT FROM MODEL
    lin_pred = 'a + b1 * cross_n_scale + b2 * dist_scale'.format(options['a'],
                                                                 options['b1'],
                                                                 options['cross_n_scale'],
                                                                 options['b2'],
                                                                 options['dist_scale'])
    p = 'exp({lin_pred})/(1+exp({lin_pred}))'.format(lin_pred)

    gscript.mapcalc(expression='{}={}'.format(options['output'], p))
    gscript.raster_history(options['output'])

    return 0


if __name__ == "__main__":
    sys.exit(main())
